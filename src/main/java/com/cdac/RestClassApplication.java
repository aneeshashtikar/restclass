package com.cdac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages= {"daoservices","controllers"})
public class RestClassApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestClassApplication.class, args);
	}

}
