package daoservices;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import pojos.User;

@Repository
@Qualifier("inmemory")
public class UserDaoService implements UserDao {

	private List<User> arr  = new ArrayList<>();
	
	
	public UserDaoService() {
		arr.add(new User("1","Aneesh",new Date()));
		arr.add(new User("2","Arun",new Date()));
		arr.add(new User("3","Aditya",new Date()));
	}
	
	@Override
	public boolean addUser(User user) {
		if(user.getId()==null) {
			user.setId((arr.size()+1)+"");
			arr.add(user);
			return true;
		}else {
			User found = findUser(user.getId());
			if(found==null) {
				return false;
			}else {
				found.setName(user.getName());
				found.setBirthdate(user.getBirthdate());
				return true;
			}
		}
	}
	
	@Override
	public User findUser(String id) {
		int i = arr.indexOf(new User(id,null,null));
		if(i==-1) {
			return null;
		}
		return arr.get(i);
	}
	
	
	
	@Override
	public boolean removeUser(String id) {
		return arr.remove(new User(id,null,null));
	}
	
	@Override
	public List<User> getAllUsers(){
		return arr;
	}
	
	
	@Override
	public boolean removeUserById(String id) {
		return arr.remove(new User(id,null,null));
	}
}
