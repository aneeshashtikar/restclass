package daoservices;

import java.util.List;

import pojos.User;

public interface UserDao {

	boolean addUser(User user);

	User findUser(String id);

	boolean removeUser(String id);

	List<User> getAllUsers();

	boolean removeUserById(String id);

}