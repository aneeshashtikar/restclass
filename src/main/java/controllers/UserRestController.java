package controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import daoservices.UserDao;
import exceptions.UserNotFoundException;
import pojos.User;

@RestController
public class UserRestController {

	@Autowired
	@Qualifier("inmemory")
	UserDao dao;
	
	@GetMapping(value="/getAllUsers")
	public ResponseEntity<List<User>> getAllUsers(){
		List<User> li =  dao.getAllUsers();
		ResponseEntity<List<User>> re =  new ResponseEntity<>(li,HttpStatus.OK);
		return re;
	}
	
	@GetMapping(value="/getUserById/{id}")
	public User getUserById(@PathVariable("id") String id) throws UserNotFoundException{
		User u = dao.findUser(id);
		if(u==null) {
			throw new UserNotFoundException();
		}
		return u;
	}
	
	
	@GetMapping(value="/removeUserById/{id}")
	public void removeUserById(@PathVariable("id") String id) throws UserNotFoundException{
		if(!dao.removeUser(id)) {
			throw new UserNotFoundException();
		}
	}
	
	@PostMapping(value="/addUser")
	public ResponseEntity<Object> addUser(@Valid @RequestBody User user) {
		if(dao.addUser(user)) {
			ResponseEntity<Object> re = new ResponseEntity<>(user,HttpStatus.CREATED);
			//re.getHeaders().add("uri","http://localhost:8080/getUserById/"+user.getId());
			return re;
		}else {
			ResponseEntity<Object> re = new ResponseEntity<>("CANNOT CREATE OR UPDATE USER",HttpStatus.BAD_REQUEST);
			return re;
		}
	}
}
